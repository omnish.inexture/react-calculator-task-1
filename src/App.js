import React, { Component } from 'react';
import "./App.css"

export default class App extends Component {

  constructor() {
    super()
    this.state = {
      arr: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
      input: ""
    }
  }

  shuffleArray = (array) => {
    for (var i = array.length - 1; i > 0; i--) {

      // Generate random number
      var j = Math.floor(Math.random() * (i + 1));

      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    this.setState({ arr: array })
  }


  onClickFun = (e) => {
    const val = e.target.value
    this.shuffleArray(this.state.arr)

    this.setState({ input: this.state.input + val })

  }

  clearScreen = () => {
    this.setState({ input: "" })
  }

  clearLastElement = () => {
    const val = this.state.input
    const valArr = val.split("")
    const newArr = valArr.slice(0, valArr.length - 1)
    const finalResult = newArr.join("")
    this.setState({ input: finalResult })

  }

  total = () => {
    try {
      const input = this.state.input
      const res = eval(input)
      this.setState({ input: String(res) })

    } catch (error) {
      this.setState({ input: error.message })
    }

  }

  render() {
    return <div className='container'>
      <input type="text" value={this.state.input} disabled />
      <div className="btnContainer">
        <button onClick={this.clearLastElement} value="C">C</button>
        <button onClick={this.clearScreen} value="CE">CE</button>
        <button onClick={this.onClickFun} value=".">.</button>
        <button onClick={this.onClickFun} value="+">+</button>
        <button onClick={this.onClickFun} value={this.state.arr[0]}>{this.state.arr[0]}</button>
        <button onClick={this.onClickFun} value={this.state.arr[1]}>{this.state.arr[1]}</button>
        <button onClick={this.onClickFun} value={this.state.arr[2]}>{this.state.arr[2]}</button>
        <button onClick={this.onClickFun} value="-">-</button>
        <button onClick={this.onClickFun} value={this.state.arr[3]}>{this.state.arr[3]}</button>
        <button onClick={this.onClickFun} value={this.state.arr[4]}>{this.state.arr[4]}</button>
        <button onClick={this.onClickFun} value={this.state.arr[5]}>{this.state.arr[5]}</button>
        <button onClick={this.onClickFun} value="*">*</button>
        <button onClick={this.onClickFun} value={this.state.arr[6]}>{this.state.arr[6]}</button>
        <button onClick={this.onClickFun} value={this.state.arr[7]}>{this.state.arr[7]}</button>
        <button onClick={this.onClickFun} value={this.state.arr[8]}>{this.state.arr[8]}</button>
        <button onClick={this.onClickFun} value="/">/</button>
        <button id='zero' onClick={this.onClickFun} value={this.state.arr[9]}>{this.state.arr[9]}</button>
        <button id='total' onClick={this.total}>=</button>
      </div>
    </div>;
  }
}
